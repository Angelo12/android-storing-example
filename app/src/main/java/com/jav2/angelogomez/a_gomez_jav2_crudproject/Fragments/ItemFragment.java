package com.jav2.angelogomez.a_gomez_jav2_crudproject.Fragments;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.jav2.angelogomez.a_gomez_jav2_crudproject.Classes.Person;

import java.util.ArrayList;

////////////////////////////////
// Angelo Jose Gomez Franzin  //
////////////////////////////////
public class ItemFragment extends ListFragment {

    public  static  final String TAG = "ItemFragment.TAG";
    public static final String ARG_PARAM1 = "param1";
    public ListToDetailDelegate delegate;


    public static ItemFragment newInstance(ArrayList<Person> persons) {
        ItemFragment fragment = new ItemFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, persons);
        fragment.setArguments(args);
        return fragment;
    }

    public interface ListToDetailDelegate {
        void listToDetails(int _id, Person _psn);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof  ListToDetailDelegate ){
            delegate = (ListToDetailDelegate) activity;
        } else  {
            throw new ClassCastException(activity.toString() + " must implement ListToDetailDelegate");
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ArrayList<Person> persons = (ArrayList<Person>) getArguments().getSerializable(ARG_PARAM1);
        ArrayAdapter<Person> personArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, persons);
        setListAdapter(personArrayAdapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Person selected = (Person) getListAdapter().getItem(position);
        delegate.listToDetails(position, selected);
    }
}
