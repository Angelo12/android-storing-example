package com.jav2.angelogomez.a_gomez_jav2_crudproject.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jav2.angelogomez.a_gomez_jav2_crudproject.Classes.Person;
import com.jav2.angelogomez.a_gomez_jav2_crudproject.R;

////////////////////////////////
// Angelo Jose Gomez Franzin  //
////////////////////////////////
public class FormFragment extends Fragment {
    public  static  final String TAG = "FormFragment.TAG";

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private int mParam1;
    private Person mParam2;

    private Boolean editing = false;

    public FormDelegate formDelegate;

    public interface FormDelegate {
        void updateObjectAndBackToDetail(int _id, Person _psn);
        void saveObjectAndBackToList(Person _psn);
    }


    public static FormFragment newInstance() {
        FormFragment fragment = new FormFragment();
        return fragment;
    }

    public static FormFragment newInstance(int param1, Person param2) {
        FormFragment fragment = new FormFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        args.putSerializable(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
            mParam2 = (Person) getArguments().getSerializable(ARG_PARAM2);

            if(!getArguments().isEmpty()) {
                editing = true;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form, container, false);

        Button add = (Button) view.findViewById(R.id.add_button_fragment);
        final EditText etFirstName = (EditText) view.findViewById(R.id.first_name_field);
        final EditText etLastName = (EditText) view.findViewById(R.id.last_name_field);
        final EditText etAge = (EditText) view.findViewById(R.id.employee_num_field);

        if (editing) {
            etFirstName.setText(mParam2.getmFirstName());
            etLastName.setText(mParam2.getmLastName());
            etAge.setText("" + mParam2.getmAge());
            add.setText("Update");
        }

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (etFirstName.getText().toString().isEmpty() || etLastName.getText().toString().isEmpty() ||
                        etAge.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Fill all the fields.", Toast.LENGTH_SHORT).show();
                } else {

                    Person newPerson = new Person(etFirstName.getText().toString(), etLastName.getText().toString(), Integer.parseInt(etAge.getText().toString()));

                    if (!editing) {
                        Log.i("Database Save:", "New record stored with id: ");
                        formDelegate.saveObjectAndBackToList(newPerson);
                    } else {
                        Log.i("Database Editing:", "Record edited with id: ");
                        formDelegate.updateObjectAndBackToDetail(mParam1, newPerson);
                    }

                    etFirstName.setText("");
                    etLastName.setText("");
                    etAge.setText("");
                }
            }
        });


        return view;

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof FormDelegate) {
          formDelegate = (FormDelegate) activity;
        } else{
            throw new IllegalArgumentException(activity.toString() + " must implement FormDelegate");
        }
    }
}
