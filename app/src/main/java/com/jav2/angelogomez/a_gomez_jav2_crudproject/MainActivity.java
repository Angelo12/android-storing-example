package com.jav2.angelogomez.a_gomez_jav2_crudproject;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.jav2.angelogomez.a_gomez_jav2_crudproject.Classes.Person;
import com.jav2.angelogomez.a_gomez_jav2_crudproject.Fragments.DetailFragment;
import com.jav2.angelogomez.a_gomez_jav2_crudproject.Fragments.FormFragment;
import com.jav2.angelogomez.a_gomez_jav2_crudproject.Fragments.ItemFragment;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

////////////////////////////////
// Angelo Jose Gomez Franzin  //
////////////////////////////////
public class MainActivity extends Activity implements DetailFragment.EditDetailsDelegate, FormFragment.FormDelegate, ItemFragment.ListToDetailDelegate {

    ArrayList<Person> loadedList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadList();

        getFragmentManager().beginTransaction().replace(R.id.frame_container, ItemFragment.newInstance(loadedList), ItemFragment.TAG)
                .addToBackStack("listBack")
                .commit();

        findViewById(R.id.main_add_button).setOnClickListener(listener);
    }

    private void loadList() {
        try {
            FileInputStream fis = openFileInput("data.bin");
            ObjectInputStream ois = new ObjectInputStream(fis);
            loadedList = (ArrayList<Person>) ois.readObject();
            ois.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveList() {
        try {
            FileOutputStream fos = openFileOutput("data.bin", this.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(loadedList);
            oos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            getFragmentManager().beginTransaction().replace(R.id.frame_container, FormFragment.newInstance(), FormFragment.TAG)
                    .addToBackStack("listBack")
                    .commit();
        }
    };


    @Override
    public void passEditDetails(int _id, Person _psn) {
        getFragmentManager().beginTransaction()
                .replace(R.id.frame_container, FormFragment.newInstance(_id, _psn), FormFragment.TAG)
                .addToBackStack("listBack")
                .commit();
    }

    @Override
    public void deleteFromDatabase(int _id) {
        loadedList.remove(_id);
        saveList();
        getFragmentManager().beginTransaction().replace(R.id.frame_container, ItemFragment.newInstance(loadedList), ItemFragment.TAG).commit();
    }

    @Override
    public void closeDetailFragment() {
        getFragmentManager().beginTransaction().replace(R.id.frame_container, ItemFragment.newInstance(loadedList), ItemFragment.TAG).commit();
    }


    @Override
    public void updateObjectAndBackToDetail(int _id, Person _psn) {
        loadedList.get(_id).equalize(_psn);
        saveList();
        getFragmentManager().beginTransaction().replace(R.id.frame_container, DetailFragment.newInstance(_id, _psn), ItemFragment.TAG).commit();
    }

    @Override
    public void saveObjectAndBackToList(Person _psn) {
        //Save Data
        loadedList.add(_psn);
        saveList();
        //Move to ItemList
        getFragmentManager().beginTransaction().replace(R.id.frame_container, ItemFragment.newInstance(loadedList), ItemFragment.TAG).commit();
    }

    @Override
    public void listToDetails(int _id, Person _psn) {
        getFragmentManager().beginTransaction().replace(R.id.frame_container, DetailFragment.newInstance(_id, _psn), DetailFragment.TAG)
                .addToBackStack("listBack")
                .commit();
    }
}
