package com.jav2.angelogomez.a_gomez_jav2_crudproject.Fragments;


import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.jav2.angelogomez.a_gomez_jav2_crudproject.Classes.Person;
import com.jav2.angelogomez.a_gomez_jav2_crudproject.R;

////////////////////////////////
// Angelo Jose Gomez Franzin  //
////////////////////////////////
public class DetailFragment extends Fragment {

    public static final  String TAG = "DetailFragment.TAG";
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    
    private int mParam1;
    private Person mParam2;

    private EditDetailsDelegate editDelegate;

    public interface EditDetailsDelegate {
        void passEditDetails(int _id, Person _psn);
        void deleteFromDatabase(int _id);
        void closeDetailFragment();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);


        if(activity instanceof EditDetailsDelegate) {
            editDelegate = (EditDetailsDelegate)activity;
        } else {
            throw new IllegalArgumentException("Activity needs to implement EditDetailsDelegate");
        }

    }

    public static DetailFragment newInstance(int param1, Person param2) {
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        args.putSerializable(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
            mParam2 = (Person) getArguments().getSerializable(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);

        TextView tvName = (TextView) view.findViewById(R.id.detail_name);
        TextView tvNumber = (TextView) view.findViewById(R.id.detail_number);

        tvName.setText("Name: " + mParam2.getmFirstName() + " " + mParam2.getmLastName());
        tvNumber.setText("Age: " + mParam2.getmAge());

        Button delete = (Button) view.findViewById(R.id.detail_delete_button);
        Button edit = (Button) view.findViewById(R.id.detail_edit_button);
        Button close = (Button) view.findViewById(R.id.detail_close_button);


        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editDelegate.deleteFromDatabase(mParam1);
            }
        });

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editDelegate.passEditDetails(mParam1, mParam2);
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               editDelegate.closeDetailFragment();
            }
        });

        return view;
    }
}
