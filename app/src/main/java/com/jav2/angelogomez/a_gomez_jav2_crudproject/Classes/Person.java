package com.jav2.angelogomez.a_gomez_jav2_crudproject.Classes;

import java.io.Serializable;

/**
 * Created by AngeloGomez on 9/9/15.
 */
public class Person implements Serializable {

    String mFirstName;
    String mLastName;
    int mAge;

    public String getmFirstName() {
        return mFirstName;
    }

    public String getmLastName() {
        return mLastName;
    }

    public int getmAge() {
        return mAge;
    }


    public Person(String mFirstName, String mLastName, int mAge) {
        this.mFirstName = mFirstName;
        this.mLastName = mLastName;
        this.mAge = mAge;
    }

    @Override
    public String toString() {
        return mFirstName + " " + mLastName;
    }

    public void equalize(Person _psn) {
        this.mFirstName = _psn.mFirstName;
        this.mLastName = _psn.getmLastName();
        this.mAge = _psn.getmAge();
    }
}
